# Orica UI Coding Challenge

Completed by Aaron Deal.

This webpage summarises the results of the Premier League 2019/2020 season found [here](https://github.com/openfootball/football.json/blob/master/2019-20/en.1.json).

## Architecture

This is a simple webpage comprised of a single HTML file, javascript file and 3 css stylesheets. No external libraries are required. 

## Running 

Open the index.html file in your preferred browser to view the results. 
