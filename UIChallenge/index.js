var results = [];
var sortField = 'rank';
var sortDirection = 'asc';

function setMessage(message) {
  document.getElementById("message").innerHTML = message;
}

function getTeamMatches(matches) {
  // Extract the individual teams from each match with thier status (win, loss, draw) and scores for/against
  var home = matches.map(function (match) {
    var score1 = match.score.ft[0];
    var score2 = match.score.ft[1];
    var compare = score1 === score2 ? 0 : score1 > score2 ? 1 : -1;
    return {
      team: match.team1,
      status: compare > 0 ? 'win' : compare < 0 ? 'loss' : 'draw',
      goalsFor: match.score.ft[0],
      goalsAgainst: match.score.ft[1]
    }
  });
  var away = matches.map(function (match) {
    var score1 = match.score.ft[0];
    var score2 = match.score.ft[1];
    var compare = score1 === score2 ? 0 : score1 > score2 ? 1 : -1;
    return {
      team: match.team2,
      status: compare < 0 ? 'win' : compare > 0 ? 'loss' : 'draw',
      goalsFor: match.score.ft[1],
      goalsAgainst: match.score.ft[0]
    }
  });
  return home.concat(away);
}

function groupTeams(teamMatches) {
  // Group each team/match result by the team and summarise required values
  var groups = teamMatches.reduce(function (p, c) {
    var team = c.team;
    p[team] = p[team] || {
      team: '',
      rank: 0,
      wins: 0,
      losses: 0,
      draws: 0,
      goalsFor: 0,
      goalsAgainst: 0,
      goalDifference: 0,
      points: 0
    };

    p[team]["team"] = c.team;

    if (c.status === 'win') {
      p[team]["wins"] = p[team]["wins"] + 1;
      p[team]["points"] = p[team]["points"] + 3; // Each win is worth 3 points
    } else if (c.status == 'draw') {
      p[team]["draws"] = p[team]["draws"] + 1;
      p[team]["points"] = p[team]["points"] + 1; // Each draw is worth 1 point
    } else {
      p[team]["losses"] = p[team]["losses"] + 1;
    }

    p[team]["goalsFor"] = p[team]["goalsFor"] + c.goalsFor;
    p[team]["goalsAgainst"] = p[team]["goalsAgainst"] + c.goalsAgainst;
    p[team]["goalDifference"] = p[team]["goalsFor"] - p[team]["goalsAgainst"];

    return p;
  }, {});

  // Map each of the teams into an array
  return Object.keys(groups).map(function (x) {
    return groups[x]
  })
}

function compareTeamRankDesc(team1, team2) {
  // Initial sort by points
  if (team1.points < team2.points) {
    return 1;
  }
  if (team1.points > team2.points) {
    return -1;
  }
  // Points are equal so check on goalDifference
  if (team1.goalDifference < team2.goalDifference) {
    return 1;
  }
  if (team1.goalDifference > team2.goalDifference) {
    return -1;
  }
  // Points and goalDifference are equal so check on goalsFor
  if (team1.goalsFor < team2.goalsFor) {
    return 1;
  }
  if (team1.goalsFor > team2.goalsFor) {
    return -1;
  }
  // All 3 values are equal
  return 0;
}

function setRank(teams) {
  // Sort by points, goalDifference, goalsFor and set the rank
  teams = teams.sort(compareTeamRankDesc);
  teams = teams.map(function (a, i) {
    a.rank = i + 1;
    return a;
  });
  return teams;
}

function sortTeams(teams) {
  // Sort by selected column for display
  var directionMod = sortDirection === 'asc' ? 1 : -1;

  return teams.sort(function (team1, team2) {
    if (team1[sortField] < team2[sortField]) {
      return -1 * directionMod;
    }
    if (team1[sortField] > team2[sortField]) {
      return 1 * directionMod;
    }

  });
}

function getTeamRow(team) {
  return '<tr>' +
    '<td>' + team.rank + '</td>' +
    '<td>' + team.team + '</td>' +
    '<td>' + team.wins + '</td>' +
    '<td>' + team.draws + '</td>' +
    '<td>' + team.losses + '</td>' +
    '<td>' + team.goalsFor + '</td>' +
    '<td>' + team.goalsAgainst + '</td>' +
    '<td>' + team.goalDifference + '</td>' +
    '<td>' + team.points + '</td>' +
    '</tr>';
}

function setSortColumnDisplay() {
  // Clear all classes
  document.getElementById('rank-head').classList.remove('sort-asc', 'sort-desc');
  document.getElementById('team-head').classList.remove('sort-asc', 'sort-desc');

  // Set the sort class on the selected column with the direction set
  switch (sortField) {
    case 'rank':
      document.getElementById('rank-head').classList.add('sort-' + sortDirection);
      break;
    case 'team':
      document.getElementById('team-head').classList.add('sort-' + sortDirection)
      break;
  }
}

function displayTeams(sortedTeams) {
  // Populate the table of values
  var content = '';
  sortedTeams.forEach(function (t) {
    content += getTeamRow(t);
  });

  var tableBody = document.getElementById('resultsTable');
  tableBody.innerHTML = content;

  setSortColumnDisplay();
}

function dataLoaded(response) {
  // On data loaded parse, summaries and sort the results
  setMessage("Data loaded");
  var obj = JSON.parse(response);
  var matches = obj.matches;
  var teamMatches = getTeamMatches(matches);
  var teamResults = groupTeams(teamMatches);
  teamResults = setRank(teamResults);
  results = sortTeams(teamResults);
  displayTeams(results);
}

function loadData() {
  // Load all data from the specified URL and set callback for when it is complete
  setMessage("Loading data...");

  var url = "https://raw.githubusercontent.com/openfootball/football.json/master/2019-20/en.1.json";

  var xmlHttp = new XMLHttpRequest();
  xmlHttp.onreadystatechange = function () {
    if (xmlHttp.readyState != 4) return;
    if (xmlHttp.status == 200)
      dataLoaded(xmlHttp.responseText);
    else
      setMessage("An error occurred loading data, " + xmlHttp.responseText);
  }
  xmlHttp.open("GET", url, true);
  xmlHttp.send(null);
}

function sortByColumn(columnName) {
  // Sort the table by the selected column, if column is already selected invert the sort direction
  if (columnName === sortField) {
    sortDirection = sortDirection === 'asc' ? 'desc' : 'asc';
  } else {
    sortDirection = 'asc';
    sortField = columnName;
  }
  results = sortTeams(results);
  displayTeams(results);
}

window.addEventListener("load", function () {
  loadData();
});