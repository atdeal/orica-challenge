
### The Task

We would like you develop a small application that can render the Premier League table.

Given the full list of matches in the season from https://github.com/openfootball/football.json/blob/master/2019-20/en.1.json, your application must output a sorted (by rank) collection containing the following information for each team:

- rank
- team name
- wins
- draws
- losses
- goals for
- goals against
- goal difference
- points

Rank is determined by points, then goal difference, then goals scored.

### What we’re looking for:

Clean, well structured code following SOLID, TDD and any design pattern usage you deem appropriate that outputs the correct results for the 2019-20 season. 
We are interested both in how you approach the problem and how you validate the output from your solution.

### Output

For a C# application
 - An easily compilable solution
 - Command line application included in the solution
 - Two renderers, both can run when the application runs
   - one for command line output as a legible table
   - the other for file output as a legible table 
 - Test assembly included in solution
 - A suitable level of code coverage
 - Limited package usage
 
For a JavaScript application
 - An easily runnable application using a single index.html and linked JS/CSS files
 - Vanilla JavaScript with no external package usage
 - No servers required
 - No transpilation or build tools required
 - All JavaScript to run client side
 - Must run in IE11
 - Styling at your discretion to improve table aesthetics
 - Rank and Team Name column should be sortable when clicking the table header
 
 
 
 
 