﻿using System;
using System.Collections.Generic;
using DataChallenge.Services;

namespace DataChallenge.Unit.TestInfrastructure
{
    public class TestConsoleService : IConsoleService
    {
        public TestConsoleService()
        {
            Messages = new List<string>();
        }

        public List<string> Messages { get; }

        public void WriteLine(string message)
        {
            Messages.Add(message);
            Console.WriteLine(message);
        }
    }
}
