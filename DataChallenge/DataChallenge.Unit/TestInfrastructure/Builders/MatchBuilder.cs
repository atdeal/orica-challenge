﻿using System;
using DataChallenge.DataModels;

namespace DataChallenge.Unit.TestInfrastructure.Builders
{
    public class MatchBuilder
    {
        private readonly Match _match;

        public MatchBuilder(string team1, string team2)
        {
            _match = new Match
            {
                Team1 = team1,
                Team2 = team2,
                Round = "Matchday 1",
                Date = new DateTime()
            };
        }

        public MatchBuilder WithRound(string value)
        {
            _match.Round = value;
            return this;
        }

        public MatchBuilder WithDate(DateTime value)
        {
            _match.Date = value;
            return this;
        }

        public MatchBuilder WithScores(int team1Score, int team2Score)
        {
            _match.Score = new MatchScore
            {
                FullTimeScore = new[] { team1Score, team2Score }
            };
            return this;
        }

        public Match Build()
        {
            return _match;
        }
    }
}
