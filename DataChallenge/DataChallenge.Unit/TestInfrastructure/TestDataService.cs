﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataChallenge.DataModels;
using DataChallenge.Services;

namespace DataChallenge.Unit.TestInfrastructure
{
    public class TestDataService : IDataService
    {
        private readonly ICollection<Match> _matches;

        public TestDataService(params Match[] matches)
        {
            _matches = matches;
        }

        public Task<List<Match>> GetMatches()
        {
            return Task.Run(() => _matches.ToList());
        }
    }
}
