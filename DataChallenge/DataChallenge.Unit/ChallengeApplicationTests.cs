﻿using System.IO;
using System.Linq;
using DataChallenge.Services;
using DataChallenge.Unit.TestInfrastructure;
using Shouldly;
using Xunit;

namespace DataChallenge.Unit
{
    public class ChallengeApplicationTests
    {
        private readonly TestConsoleService _console;
        private readonly ChallengeApplication _application;

        public ChallengeApplicationTests()
        {
            _console = new TestConsoleService();
            var dataService = new OpenFootballDataService();
            var summaryService = new TeamSummaryService(dataService);
            _application = new ChallengeApplication(_console, summaryService);
        }

        [Fact]
        public async void GivenCommandArgument_WhenRun_ShouldOutputToCommandLine()
        {
            // Arrange
            var dataService = new OpenFootballDataService();
            var summaryService = new TeamSummaryService(dataService);
            var application = new ChallengeApplication(_console, summaryService);

            // Act
            await application.Run("command");

            // Assert
            _console.Messages.Count.ShouldBe(21);

            CheckColumnHeaders(_console.Messages[0]);

            // Check the top result
            _console.Messages[1].Replace(" ", "").ShouldBe("1\tLiverpoolFC\t32\t3\t3\t85\t33\t52\t99");
        }

        [Fact]
        public async void GivenNoArgument_WhenRun_ShouldOutputToCommandLineByDefault()
        {
            // Act
            await _application.Run(null);

            // Assert
            _console.Messages.Count.ShouldBe(21);

            CheckColumnHeaders(_console.Messages[0]);

            // Check the top result
            _console.Messages[1].Replace(" ", "").ShouldBe("1\tLiverpoolFC\t32\t3\t3\t85\t33\t52\t99");
        }

        [Fact]
        public async void GivenFileArgument_WhenRun_ShouldOutputToFileInCsvFormat()
        {
            // Arrange
            var path = Path.GetTempFileName();

            // Act
            await _application.Run("file", path);

            // Assert
            _console.Messages.Count.ShouldBe(1);
            _console.Messages.Single().ShouldContain($"Results saved to '{path}'");

            var lines = File.ReadAllLines(path);

            lines.Length.ShouldBe(21);

            CheckColumnHeaders(lines[0]);

            // Check the top result
            lines[1].ShouldBe("1,Liverpool FC,32,3,3,85,33,52,99");
        }

        private static void CheckColumnHeaders(string header)
        {
            header.ShouldContain("Rank");
            header.ShouldContain("TeamName");
            header.ShouldContain("Wins");
            header.ShouldContain("Draws");
            header.ShouldContain("Losses");
            header.ShouldContain("GoalsFor");
            header.ShouldContain("GoalsAgainst");
            header.ShouldContain("GoalDifference");
            header.ShouldContain("Points");
        }
    }
}
