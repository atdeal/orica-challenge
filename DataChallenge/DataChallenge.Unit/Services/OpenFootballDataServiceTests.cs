using System;
using DataChallenge.Services;
using Shouldly;
using Xunit;

namespace DataChallenge.Unit.Services
{
    public class OpenFootballDataServiceTests
    {
        [Fact]
        public async void GivenOpenFootballService_GetMatches_ShouldReturnAllMatchesInCorrectModel()
        {
            // Arrange
            var service = new OpenFootballDataService();

            // Act
            var result = await service.GetMatches();

            // Assert
            result.Count.ShouldBe(380);

            // Confirm first team matches model
            var first = result[0];
            first.Round.ShouldBe("Matchday 1");
            first.Date.ShouldBe(new DateTime(2019, 08, 09));
            first.Team1.ShouldBe("Liverpool FC");
            first.Team2.ShouldBe("Norwich City FC");
            first.Score.FullTimeScore[0].ShouldBe(4);
            first.Score.FullTimeScore[1].ShouldBe(1);
        }
    }
}
