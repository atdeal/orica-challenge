using System.Linq;
using DataChallenge.Services;
using DataChallenge.Unit.TestInfrastructure;
using DataChallenge.Unit.TestInfrastructure.Builders;
using Shouldly;
using Xunit;

namespace DataChallenge.Unit.Services
{
    public class TeamSummaryServiceTests
    {
        [Fact]
        public async void GivenAllMatches_WhenGetTeamSummaries_ShouldReturnAllTeamsInCorrectOrder()
        {
            // Arrange
            var dataService = new OpenFootballDataService();
            var service = new TeamSummaryService(dataService);

            // Act
            var result = await service.GetTeamSummaries();

            // Assert
            result.Count.ShouldBe(20);

            result.Select(r => r.Rank).ShouldBeInOrder();

            // Test model of first team is correct
            var team1Result = result[0];
            team1Result.Rank.ShouldBe(1);
            team1Result.TeamName.ShouldBe("Liverpool FC");
            team1Result.Wins.ShouldBe(32);
            team1Result.Draws.ShouldBe(3);
            team1Result.Losses.ShouldBe(3);
            team1Result.GoalsFor.ShouldBe(85);
            team1Result.GoalsAgainst.ShouldBe(33);
            team1Result.GoalDifference.ShouldBe(52);
            team1Result.Points.ShouldBe(99);

            // Note: Results from https://www.premierleague.com/tables?co=1&se=274&ha=-1 with Season filter 2019/20
        }

        [Fact]
        public async void GivenMatchesForTeam1_WhenGetTeamSummaries_ShouldReturnCorrectModelForTeam1()
        {
            // Arrange
            var team1Name = "team1";

            var match1 = new MatchBuilder(team1Name, "team2")
                .WithScores(4, 1)
                .Build();
            var match2 = new MatchBuilder(team1Name, "team3")
                .WithScores(0, 1)
                .Build();
            var match3 = new MatchBuilder(team1Name, "team4")
                .WithScores(2, 0)
                .Build();
            var match4 = new MatchBuilder(team1Name, "team5")
                .WithScores(1, 1)
                .Build();

            var dataService = new TestDataService(match1, match2, match3, match4);
            var service = new TeamSummaryService(dataService);

            // Act
            var result = await service.GetTeamSummaries();

            // Assert
            result.Count.ShouldBe(5);

            var team1Result = result[0];
            team1Result.Rank.ShouldBe(1);
            team1Result.TeamName.ShouldBe(team1Name);
            team1Result.Wins.ShouldBe(2);
            team1Result.Draws.ShouldBe(1);
            team1Result.Losses.ShouldBe(1);
            team1Result.GoalsFor.ShouldBe(7);
            team1Result.GoalsAgainst.ShouldBe(3);
            team1Result.GoalDifference.ShouldBe(4);
            team1Result.Points.ShouldBe(7);

            result[1].Rank.ShouldBe(2);
            result[1].TeamName.ShouldBe(match2.Team2);
            result[2].Rank.ShouldBe(3);
            result[2].TeamName.ShouldBe(match4.Team2);
            result[3].Rank.ShouldBe(4);
            result[3].TeamName.ShouldBe(match3.Team2);
            result[4].Rank.ShouldBe(5);
            result[4].TeamName.ShouldBe(match1.Team2);
        }

        [Fact]
        public async void GivenTeamsTiedForPoints_WhenGetTeamSummaries_ShouldOrderByGoalDifference()
        {
            // Arrange
            var team1Name = "team1";
            var team2Name = "team2";

            var match1 = new MatchBuilder(team1Name, "team3")
                .WithScores(3, 1)
                .Build();
            var match2 = new MatchBuilder(team2Name, "team3")
                .WithScores(4, 1)
                .Build();

            var dataService = new TestDataService(match1, match2);
            var service = new TeamSummaryService(dataService);

            // Act
            var result = await service.GetTeamSummaries();

            // Assert
            result.Count.ShouldBe(3);

            result[0].Rank.ShouldBe(1);
            result[0].TeamName.ShouldBe(team2Name);
            result[1].Rank.ShouldBe(2);
            result[1].TeamName.ShouldBe(team1Name);
        }

        [Fact]
        public async void GivenTeamsTiedForPointsAndGoalDifference_WhenGetTeamSummaries_ShouldOrderByGoalsScored()
        {
            // Arrange
            var team1Name = "team1";
            var team2Name = "team2";

            var match1 = new MatchBuilder(team1Name, "team3")
                .WithScores(3, 1)
                .Build();
            var match2 = new MatchBuilder(team2Name, "team3")
                .WithScores(4, 2)
                .Build();

            var dataService = new TestDataService(match1, match2);
            var service = new TeamSummaryService(dataService);

            // Act
            var result = await service.GetTeamSummaries();

            // Assert
            result.Count.ShouldBe(3);

            result[0].Rank.ShouldBe(1);
            result[0].TeamName.ShouldBe(team2Name);
            result[1].Rank.ShouldBe(2);
            result[1].TeamName.ShouldBe(team1Name);
        }
    }
}
