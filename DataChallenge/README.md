# Orica Coding Challenge

Completed by Aaron Deal.

This application summarises the results of the Premier League 2019/2020 season found [here](https://github.com/openfootball/football.json/blob/master/2019-20/en.1.json).

## Architecture

This application runs as console application, however is structured to be used in any .Net Core application such as a web API using the TeamSummaryService.

####  TeamSummaryService
The team summary service is the primary service, utilising the data service to load in all required data and summarise each teams matches.

#### DataService
The data service is responsible for loading the data from the source and converting it into the correct model (DataModel.Match.cs).

## Building & Running Solution

The .NET Core CLI, which is installed with [the .NET Core SDK](https://www.microsoft.com/net/download) is required to build the application.

To output results to the command line use the 'command' parameter as below:

```shell
dotnet build
dotnet run command
```

To output results as a csv file line use the 'file' parameter and specify the file path as below:

```shell
dotnet build
dotnet run file "c:\test\test.csv"
```

## Tests

Tests are included for all major services and the application method. They can be run using the built in .NET Core test runner.

```shell
dotnet test
```
