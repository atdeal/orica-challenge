﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataChallenge.DataModels;
using DataChallenge.Models;

namespace DataChallenge.Services
{
    public class TeamSummaryService : ITeamSummaryService
    {
        private readonly IDataService _dataService;

        public TeamSummaryService(IDataService dataService)
        {
            _dataService = dataService;
        }

        public async Task<List<TeamSummary>> GetTeamSummaries()
        {
            // Get data for each match in the season
            var matches = await _dataService.GetMatches();

            // Pull each team from each match with status and goals for/against
            var teamMatches = matches.SelectMany(m => new List<TeamMatch>
            {
                new TeamMatch(m, true),
                new TeamMatch(m, false),
            });

            // Group and summarise each team
            var teams = teamMatches.GroupBy(t => t.Team)
                .Select(t => new TeamSummary
                {
                    TeamName = t.Key,
                    Wins = t.Count(a => a.Status == Status.Win),
                    Draws = t.Count(a => a.Status == Status.Draw),
                    Losses = t.Count(a => a.Status == Status.Loss),
                    GoalsFor = t.Sum(a => a.GoalsFor),
                    GoalsAgainst = t.Sum(a => a.GoalsAgainst),
                }).ToList();

            // Sort into rank and set rank field
            var teamsSorted = teams
                .OrderByDescending(t => t.Points)
                .ThenByDescending(t => t.GoalDifference)
                .ThenByDescending(t => t.GoalsFor)
                .ToList();

            for (var index = 0; index < teamsSorted.Count(); index++)
            {
                teamsSorted[index].Rank = index + 1;
            }

            return teamsSorted;
        }

        private class TeamMatch
        {
            public string Team { get; }
            public Status Status { get; }
            public int GoalsFor { get; }
            public int GoalsAgainst { get; }

            public TeamMatch(Match match, bool home)
            {
                var compare = match.Score.FullTimeScore[0].CompareTo(match.Score.FullTimeScore[1]);
                if (home)
                {
                    Team = match.Team1;
                    Status = compare > 0 ? Status.Win : compare < 0 ? Status.Loss : Status.Draw;
                    GoalsFor = match.Score.FullTimeScore[0];
                    GoalsAgainst = match.Score.FullTimeScore[1];
                }
                else
                {
                    Team = match.Team2;
                    Status = compare < 0 ? Status.Win : compare > 0 ? Status.Loss : Status.Draw;
                    GoalsFor = match.Score.FullTimeScore[1];
                    GoalsAgainst = match.Score.FullTimeScore[0];
                }
            }
        }

        private enum Status
        {
            Win,
            Loss,
            Draw
        }
    }
}
