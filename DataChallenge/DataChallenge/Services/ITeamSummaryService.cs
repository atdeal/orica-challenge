﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataChallenge.Services
{
    public interface ITeamSummaryService
    {
        Task<List<Models.TeamSummary>> GetTeamSummaries();
    }
}
