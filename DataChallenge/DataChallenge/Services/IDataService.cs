﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DataChallenge.DataModels;

namespace DataChallenge.Services
{
    public interface IDataService
    {
        Task<List<Match>> GetMatches();
    }
}
