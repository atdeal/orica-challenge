﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using DataChallenge.DataModels;

namespace DataChallenge.Services
{
    public class OpenFootballDataService : IDataService
    {
        private const string Url = @"https://raw.githubusercontent.com/openfootball/football.json/master/2019-20/en.1.json";

        public async Task<List<Match>> GetMatches()
        {
            using (var httpClient = new HttpClient())
            using (var response = await httpClient.GetAsync(Url))
            {
                response.EnsureSuccessStatusCode();

                using (var content = response.Content)
                {
                    var rawResult = await content.ReadAsStringAsync();
                    var result = JsonSerializer.Deserialize<MatchSet>(rawResult);
                    return result?.Matches.ToList();
                }
            }
        }
    }
}
