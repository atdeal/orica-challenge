﻿namespace DataChallenge.Services
{
    public interface IConsoleService
    {
        void WriteLine(string message);
    }
}
