﻿using System;

namespace DataChallenge.Services
{
    public class ConsoleService : IConsoleService
    {
        public void WriteLine(string message)
        {
            Console.WriteLine(message);
        }
    }
}
