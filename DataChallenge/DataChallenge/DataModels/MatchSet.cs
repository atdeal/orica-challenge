﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace DataChallenge.DataModels
{
    public class MatchSet
    {
        [JsonPropertyName("name")] public string Name { get; set; }
        [JsonPropertyName("matches")] public ICollection<Match> Matches { get; set; }
    }
}
