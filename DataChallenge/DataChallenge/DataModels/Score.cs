﻿using System.Text.Json.Serialization;

namespace DataChallenge.DataModels
{
    public class MatchScore
    {
        [JsonPropertyName("ft")] public int[] FullTimeScore { get; set; }
    }
}
