﻿using System;
using System.Text.Json.Serialization;

namespace DataChallenge.DataModels
{
    public class Match
    {
        [JsonPropertyName("round")] public string Round { get; set; }
        [JsonPropertyName("date")] public DateTime Date { get; set; }
        [JsonPropertyName("team1")] public string Team1 { get; set; }
        [JsonPropertyName("team2")] public string Team2 { get; set; }
        [JsonPropertyName("score")] public MatchScore Score { get; set; }
    }
}
