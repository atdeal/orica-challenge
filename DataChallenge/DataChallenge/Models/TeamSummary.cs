﻿using System;
using System.Text;

namespace DataChallenge.Models
{
    public class TeamSummary
    {
        public int Rank { get; set; }
        public string TeamName { get; set; }
        public int Wins { get; set; }
        public int Draws { get; set; }
        public int Losses { get; set; }
        public int GoalsFor { get; set; }
        public int GoalsAgainst { get; set; }
        public int GoalDifference => GoalsFor - GoalsAgainst;
        public int Points => Wins * 3 + Draws;

        public string ToString(string separator, int teamNameLength, bool padCells)
        {
            var sb = new StringBuilder();
            sb.Append(GetFieldWithPadding(nameof(Rank), padCells, separator));
            sb.Append(separator);
            sb.Append($"{TeamName}".PadRight(teamNameLength));
            sb.Append(separator);
            sb.Append(GetFieldWithPadding(nameof(Wins), padCells, separator));
            sb.Append(separator);
            sb.Append(GetFieldWithPadding(nameof(Draws), padCells, separator));
            sb.Append(separator);
            sb.Append(GetFieldWithPadding(nameof(Losses), padCells, separator));
            sb.Append(separator);
            sb.Append(GetFieldWithPadding(nameof(GoalsFor), padCells, separator));
            sb.Append(separator);
            sb.Append(GetFieldWithPadding(nameof(GoalsAgainst), padCells, separator));
            sb.Append(separator);
            sb.Append(GetFieldWithPadding(nameof(GoalDifference), padCells, separator));
            sb.Append(separator);
            sb.Append(GetFieldWithPadding(nameof(Points), padCells, separator));
            return sb.ToString();
        }

        private string GetFieldWithPadding(string name, bool pad, string separator)
        {
            var field = GetType().GetProperty(name);
            var value = field?.GetValue(this)?.ToString();
            if (value == null) throw new InvalidOperationException("Invalid field name");
            if (value.Contains(separator))
                value = $"'{value}";
            return !pad ? value : value.PadRight(name.Length);
        }

        public static string GetHeader(string separator, int teamNameLength)
        {
            var sb = new StringBuilder();
            sb.Append(nameof(Rank));
            sb.Append(separator);
            sb.Append(nameof(TeamName).PadRight(teamNameLength));
            sb.Append(separator);
            sb.Append(nameof(Wins));
            sb.Append(separator);
            sb.Append(nameof(Draws));
            sb.Append(separator);
            sb.Append(nameof(Losses));
            sb.Append(separator);
            sb.Append(nameof(GoalsFor));
            sb.Append(separator);
            sb.Append(nameof(GoalsAgainst));
            sb.Append(separator);
            sb.Append(nameof(GoalDifference));
            sb.Append(separator);
            sb.Append(nameof(Points));
            return sb.ToString();
        }
    }
}
