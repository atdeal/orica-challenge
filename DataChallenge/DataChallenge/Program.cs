﻿using System;
using System.Threading.Tasks;
using DataChallenge.Services;
using Microsoft.Extensions.DependencyInjection;

namespace DataChallenge
{
    internal static class Program
    {
        private static async Task Main(string[] args)
        {
            // Create service collection and configure our services for dependency injection
            IServiceCollection services = new ServiceCollection();

            services.AddTransient<IConsoleService, ConsoleService>();
            services.AddTransient<IDataService, OpenFootballDataService>();
            services.AddTransient<ITeamSummaryService, TeamSummaryService>();
            services.AddTransient<ChallengeApplication>();

            // Generate a provider
            var serviceProvider = services.BuildServiceProvider();

            // Start the application
            var application = serviceProvider.GetService<ChallengeApplication>();
            if (application == null)
                throw new InvalidOperationException();

            await application.Run(args);
        }
    }
}
