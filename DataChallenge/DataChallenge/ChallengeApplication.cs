﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DataChallenge.Models;
using DataChallenge.Services;

namespace DataChallenge
{
    public class ChallengeApplication
    {
        private readonly IConsoleService _console;
        private readonly ITeamSummaryService _summaryService;

        public ChallengeApplication(IConsoleService console, ITeamSummaryService summaryService)
        {
            _console = console;
            _summaryService = summaryService;
        }

        public async Task Run(params string[] args)
        {
            var result = await _summaryService.GetTeamSummaries();

            if (args != null && args.Length > 0 && args[0] == "file")
            {
                if (args.Length < 2)
                {
                    Console.WriteLine("ERROR: No file path specified");
                    return;
                }

                var path = args[1];

                using (var sw = new StreamWriter(path))
                {
                    const string separator = ",";
                    var header = TeamSummary.GetHeader(separator, 0);
                    sw.WriteLine(header);
                    foreach (var res in result)
                        sw.WriteLine(res.ToString(separator, 0, false));
                    sw.Close();
                }

                _console.WriteLine($"Results saved to '{path}'");
            }
            else
            {
                const string separator = "\t";

                var maxTeamLength = result.Max(r => r.TeamName.Length);

                var header = TeamSummary.GetHeader(separator, maxTeamLength);
                _console.WriteLine(header);

                foreach (var res in result)
                    _console.WriteLine(res.ToString(separator, maxTeamLength, true));
            }
        }
    }
}
